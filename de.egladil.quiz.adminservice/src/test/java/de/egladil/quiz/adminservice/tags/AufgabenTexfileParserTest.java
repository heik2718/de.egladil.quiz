//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.tags;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.egladil.quiz.persistence.exceptions.QuizException;

/**
 * AufgabenTexfileParserTest
 */
public class AufgabenTexfileParserTest {

	@Test
	void parse2009() {

		try (InputStream in = getClass().getResourceAsStream("/2009_aufgaben.tex")) {

			final AufgabenTexfileParser parser = new AufgabenTexfileParser();

			final List<SchluesselUndNummer> result = parser.getAufgaben(in);

			assertEquals(15, result.size());
			for (final SchluesselUndNummer schluesselUndNummer : result) {
				assertNotNull(schluesselUndNummer.getSchluessel());
				assertNotNull(schluesselUndNummer.getNummer());
				System.out.println(schluesselUndNummer);
			}

		} catch (final IOException e) {
			fail(e.getMessage());
		}

	}

	@Test
	void parse2014() {

		try (InputStream in = getClass().getResourceAsStream("/2014_aufgaben.tex")) {

			final AufgabenTexfileParser parser = new AufgabenTexfileParser();

			final List<SchluesselUndNummer> result = parser.getAufgaben(in);

			assertEquals(15, result.size());
			for (final SchluesselUndNummer schluesselUndNummer : result) {
				assertNotNull(schluesselUndNummer.getSchluessel());
				assertNotNull(schluesselUndNummer.getNummer());
				System.out.println(schluesselUndNummer);
			}

		} catch (final IOException e) {
			fail(e.getMessage());
		}

	}

	@Test
	void parse2018Klasse1() {

		try (InputStream in = getClass().getResourceAsStream("/2018_aufgaben_klasse1.tex")) {

			final AufgabenTexfileParser parser = new AufgabenTexfileParser();

			final List<SchluesselUndNummer> result = parser.getAufgaben(in);

			assertEquals(12, result.size());
			for (final SchluesselUndNummer schluesselUndNummer : result) {
				assertNotNull(schluesselUndNummer.getSchluessel());
				assertNotNull(schluesselUndNummer.getNummer());
				System.out.println(schluesselUndNummer);
			}

		} catch (final IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void parse2018Klasse2() {

		try (InputStream in = getClass().getResourceAsStream("/2018_aufgaben_klasse2.tex")) {

			final AufgabenTexfileParser parser = new AufgabenTexfileParser();

			final List<SchluesselUndNummer> result = parser.getAufgaben(in);

			assertEquals(15, result.size());
			for (final SchluesselUndNummer schluesselUndNummer : result) {
				assertNotNull(schluesselUndNummer.getSchluessel());
				assertNotNull(schluesselUndNummer.getNummer());
				System.out.println(schluesselUndNummer);
			}

		} catch (final IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void parseFehlerhaft() {

		try (InputStream in = getClass().getResourceAsStream("/fehlerhaft.tex")) {

			final AufgabenTexfileParser parser = new AufgabenTexfileParser();

			// Act + Assert
			final Throwable exception = assertThrows(QuizException.class, () -> {
				parser.getAufgaben(in);
			});
			assertEquals("Fehler in Datei: konnte Schlüssel nicht ermitteln.", exception.getMessage());

		} catch (final IOException e) {
			fail(e.getMessage());
		}
	}
}
