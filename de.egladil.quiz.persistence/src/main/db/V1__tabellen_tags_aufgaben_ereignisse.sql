CREATE TABLE `aufgaben` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SCHLUESSEL` varchar(6) COLLATE utf8_unicode_ci NOT NULL COMMENT 'fachlicher Schlüssel der Aufgabe (Name der Dateien)',
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_aufgaben_1` (`SCHLUESSEL`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `tags` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'fachlicher Schlüssel des tags, Semantik für die Daten also Tagwerte',
  `KOMMENTAR` varchar(250) COLLATE utf8_unicode_ci COMMENT 'optionaler Kommentar',
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_tags_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tagwerte` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TAG_ID` int(10) unsigned DEFAULT NULL COMMENT 'Referenz auf das TAG',
  `WERT` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Element der Wertemenge eines Tags, Daten',
  `KOMMENTAR` varchar(250) COLLATE utf8_unicode_ci COMMENT 'optionaler Kommentar',
  `VERSION` int(10) NOT NULL DEFAULT '0',
  `DATE_MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_tagwerte_1` (`TAG_ID`, `WERT`),
  CONSTRAINT `fk_tagwert_tag` FOREIGN KEY (`TAG_ID`) REFERENCES `tags` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `aufgaben_tagwerte` (
  `AUFGABE_ID` int(10) unsigned NOT NULL,
  `TAGWERT_ID` int(10) unsigned NOT NULL,
  KEY `fk_aufgtgwrt_aufgabe` (`AUFGABE_ID`),
  KEY `fk_aufgtgwrt_tagwert` (`TAGWERT_ID`),
  UNIQUE KEY `uk_aufgaben_tagwerte_1` (`AUFGABE_ID`,`TAGWERT_ID`),
  CONSTRAINT `fk_aufgtgwrt_aufgabe` FOREIGN KEY (`AUFGABE_ID`) REFERENCES `aufgaben` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_aufgtgwrt_tagwert` FOREIGN KEY (`TAGWERT_ID`) REFERENCES `tagwerte` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ereignisarten` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `KUERZEL` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `BESCHREIBUNG` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUFBEWAHREN` int(1) NOT NULL DEFAULT '0' COMMENT 'Flag, ob Ereignisse mit diesem Typ Zwecks Nachweispflich DSGVO länger als 2 Wochen aufbewahrt werden.',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uk_ereignisarten_1` (`KUERZEL`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ereignisse` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EREIGNISART` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `WANN` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `WER` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `WAS` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(10) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `fk_ereigniss_ereignisart` (`EREIGNISART`)
) ENGINE=InnoDB AUTO_INCREMENT=5350 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



