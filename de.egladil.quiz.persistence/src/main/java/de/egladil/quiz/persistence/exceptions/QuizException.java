//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.exceptions;

/**
 * QuizException
 */
public class QuizException extends RuntimeException {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von QuizException
	 */
	public QuizException(final String message) {

		super(message);
	}

	/**
	 * Erzeugt eine Instanz von QuizException
	 */
	public QuizException(final Throwable cause) {

		super(cause);
	}

	/**
	 * Erzeugt eine Instanz von QuizException
	 */
	public QuizException(final String message, final Throwable cause) {

		super(message, cause);
	}

	/**
	 * Erzeugt eine Instanz von QuizException
	 */
	public QuizException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {

		super(message, cause, enableSuppression, writableStackTrace);
	}

}
