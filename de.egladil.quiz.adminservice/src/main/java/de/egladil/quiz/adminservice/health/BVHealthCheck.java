//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.health;

import javax.persistence.PersistenceException;

import com.codahale.metrics.health.HealthCheck;
import com.google.inject.Inject;

import de.egladil.bv.aas.domain.Role;
import de.egladil.bv.aas.storage.IRollenDao;

/**
 * BVHealthCheck
 */
public class BVHealthCheck extends HealthCheck {

	private final IRollenDao rollenDao;

	/**
	 * Erzeugt eine Instanz von BVHealthCheck
	 */
	@Inject
	public BVHealthCheck(final IRollenDao rollenDao) {
		this.rollenDao = rollenDao;
	}

	@Override
	protected Result check() throws Exception {
		try {
			rollenDao.findByRole(Role.MKV_ADMIN);
			return Result.healthy();
		} catch (final PersistenceException e) {
			return Result.unhealthy("BV not healthy: " + e.getMessage());
		}
	}
}
