//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * TagwertTest
 */
public class TagwertTest {

	@Nested
	@DisplayName("Tests für equals")
	class EqualsTests {

		@Test
		@DisplayName("both werte null, both tag names null")
		void nullWerteSameTagsEqual() {
			final Tag tag1 = new Tag();
			final Tag tag2 = new Tag();

			final Tagwert tagwert1 = new Tagwert();
			final Tagwert tagwert2 = new Tagwert();

			tag1.addWert(tagwert1);
			tag2.addWert(tagwert2);

			assertTrue(tagwert1.equals(tagwert2));
			assertEquals(tagwert1.hashCode(), tagwert2.hashCode());
		}

		@Test
		@DisplayName("both werte null, same tag")
		void nullWerteSameTags() {
			final Tag tag1 = new Tag();

			final Tagwert tagwert1 = new Tagwert();
			final Tagwert tagwert2 = new Tagwert();

			tag1.addWert(tagwert1);
			tagwert2.onAddToTag(tag1);

			assertTrue(tagwert1.equals(tagwert2));
			assertEquals(tagwert1.hashCode(), tagwert2.hashCode());
		}

		@SuppressWarnings("unlikely-arg-type")
		@Test
		@DisplayName("uncomparable classes")
		void notSameClass() {
			final Tagwert tagwert = new Tagwert();
			tagwert.setId(1l);

			assertFalse(tagwert.equals(new Aufgabe()));
		}

		@Test
		@DisplayName("first wert null, other wert not null, same tag")
		void firstWertNullOtherWertNotNull() {
			final Tag tag1 = new Tag();

			final Tagwert tagwert1 = new Tagwert();
			final Tagwert tagwert2 = new Tagwert();
			tagwert2.setWert("gasuiHgxsia");

			tag1.addWert(tagwert1);
			tagwert2.onAddToTag(tag1);

			assertFalse(tagwert1.equals(tagwert2));
		}

		@Test
		@DisplayName("first wert null, other wert not null, same tag")
		void firstWertNotNullOtherWertNull() {
			final Tag tag1 = new Tag();

			final Tagwert tagwert1 = new Tagwert();
			final Tagwert tagwert2 = new Tagwert();
			tagwert1.setWert("gasuiHgxsia");

			tag1.addWert(tagwert1);
			tagwert2.onAddToTag(tag1);

			assertFalse(tagwert1.equals(tagwert2));
		}

		@Test
		@DisplayName("first wert null, other wert not null, same tag")
		void otherTagwertNull() {
			final Tag tag1 = new Tag();

			final Tagwert tagwert1 = new Tagwert();
			tagwert1.setWert("gasuiHgxsia");

			tag1.addWert(tagwert1);

			assertFalse(tagwert1.equals(null));
		}

		@Test
		@DisplayName("completely different werte, same tag")
		void differentWerte() {
			final Tag tag1 = new Tag();

			final Tagwert tagwert1 = new Tagwert();
			final Tagwert tagwert2 = new Tagwert();
			tagwert1.setWert("gasuiHgxsia");
			tagwert2.setWert("ldhfiah");

			tag1.addWert(tagwert1);
			tagwert2.onAddToTag(tag1);

			assertFalse(tagwert1.equals(tagwert2));
		}

		@Test
		@DisplayName("completely same werte, same tag")
		void sameWerte() {
			final Tag tag1 = new Tag();

			final Tagwert tagwert1 = new Tagwert();
			final Tagwert tagwert2 = new Tagwert();
			tagwert1.setWert("gasuiHgxsia");
			tagwert2.setWert("gasuiHgxsia");

			tag1.addWert(tagwert1);
			tagwert2.onAddToTag(tag1);

			assertTrue(tagwert1.equals(tagwert2));
			assertEquals(tagwert1.hashCode(), tagwert2.hashCode());
		}

		@Test
		@DisplayName("case insensitive same werte, same tag")
		void sameWerteCI() {
			final Tag tag1 = new Tag();

			final Tagwert tagwert1 = new Tagwert();
			final Tagwert tagwert2 = new Tagwert();
			tagwert1.setWert("gasuiHgxsia");
			tagwert2.setWert("gasuihgxsia");

			tag1.addWert(tagwert1);
			tagwert2.onAddToTag(tag1);

			assertTrue(tagwert1.equals(tagwert2));
			assertEquals(tagwert1.hashCode(), tagwert2.hashCode());
		}

		@Test
		@DisplayName("same werte, different tags")
		void sameWerteDifferentTags() {
			final Tag tag1 = new Tag();
			tag1.setName("sah");

			final Tag tag2 = new Tag();
			tag2.setName("fuu");


			final Tagwert tagwert1 = new Tagwert();
			final Tagwert tagwert2 = new Tagwert();
			tagwert1.setWert("gasuiHgxsia");
			tagwert2.setWert("gasuihgxsia");

			tag1.addWert(tagwert1);
			tag2.addWert(tagwert2);

			assertFalse(tagwert1.equals(tagwert2));
		}
	}
}
