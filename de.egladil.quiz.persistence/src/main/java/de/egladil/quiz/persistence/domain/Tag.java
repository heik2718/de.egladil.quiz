//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import de.egladil.common.persistence.IDomainObject;

/**
 * Tag
 */
@Entity
@Table(name = "tags")
public class Tag implements IDomainObject {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Version
	@Column(name = "VERSION")
	private int version;

	@Column(name = "NAME")
	private String name;

	@Column(name = "KOMMENTAR")
	private String kommentar;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tag", cascade = { CascadeType.ALL })
	private List<Tagwert> werte = new ArrayList<>();

	/**
	 * Tag
	 */
	public Tag() {
	}

	/**
	 * Tag
	 */
	public Tag(final String name) {
		this.name = name;
	}

	public void addWert(final Tagwert wert) {
		if (wert == null) {
			throw new IllegalArgumentException("wert null");
		}
		if (!werte.contains(wert)) {
			werte.add(wert);
			wert.onAddToTag(this);
		}
	}

	public void removeWert(final Tagwert wert) {
		if (wert == null) {
			throw new IllegalArgumentException("wert null");
		}
		if (werte.contains(wert)) {
			werte.remove(wert);
			wert.onRemoveFromTag();
		}
	}

	/**
	 * Sucht caseinsensitiv nach den Tagwert mit dem gegebenen Wert.
	 *
	 * @param wert String darf nicht null sein.
	 * @return Optional
	 */
	public Optional<Tagwert> findWert(final String wert) {
		if (wert == null) {
			throw new NullPointerException("wert");
		}
		return werte.stream().filter(w -> w.getWert().toLowerCase().equals(wert.toLowerCase())).findFirst();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	public final void setId(final Long id) {
		this.id = id;
	}

	public final String getName() {
		return name;
	}

	public final void setName(final String name) {
		this.name = name;
	}

	public final String getKommentar() {
		return kommentar;
	}

	public final void setKommentar(final String kommentar) {
		this.kommentar = kommentar;
	}

	public final List<Tagwert> getWerte() {
		return werte;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.toLowerCase().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Tag other = (Tag) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else {
			if (other.name == null) {
				return false;
			} else {
				if (!name.toLowerCase().equals(other.name.toLowerCase())) {
					return false;
				}
			}
		}
		return true;
	}
}
