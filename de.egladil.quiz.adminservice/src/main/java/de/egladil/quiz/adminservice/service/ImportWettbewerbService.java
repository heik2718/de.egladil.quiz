//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.service;

import java.io.IOException;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.quiz.persistence.domain.Klassenstufe;
import de.egladil.quiz.persistence.exceptions.QuizException;

/**
 * ImportWettbewerbService
 */
public interface ImportWettbewerbService {

	/**
	 * Importiert alle Aufgaben aus einer LaTeX-Datei und erzeugt alle Tagwerte, die sich automatisch ermitteln lassen.
	 *
	 * @param wettbewerbsjahr String das Jahr, für das die Aufgaben importiert werden
	 * @param klassenstufe Klassenstufe die Klassenstufe, für die die Aufgaben importiert werden
	 * @param dateiname String der Name der LaTeX- Datei
	 * @throws PreconditionFailedException falls diese Klassenstufe und dieses Jahr bereits importiert wurden
	 * @throws IOException falls sich die Datei nicht finden oder lesen lässt
	 * @throws QuizException falls es Parse-Fehler gibt
	 */
	void importAufgaben(String wettbewerbsjahr, Klassenstufe klassenstufe, String dateiname)
		throws PreconditionFailedException, QuizException, IOException;

}
