//=====================================================
// Projekt: de.egladil.quit.tests
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quit.tests.adminservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.inject.Inject;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.quit.tests.AbstractGuiceIT;
import de.egladil.quiz.adminservice.service.ImportWettbewerbService;
import de.egladil.quiz.persistence.dao.IAufgabeDao;
import de.egladil.quiz.persistence.dao.ITagDao;
import de.egladil.quiz.persistence.domain.Aufgabe;
import de.egladil.quiz.persistence.domain.Klassenstufe;
import de.egladil.quiz.persistence.domain.Tag;
import de.egladil.quiz.persistence.exceptions.QuizException;

/**
 * ImportWettbewerbServiceImplIT
 */
public class ImportWettbewerbServiceImplIT extends AbstractGuiceIT {

	@Inject
	private ImportWettbewerbService service;

	@Inject
	private IAufgabeDao aufgabeDao;

	@Inject
	private ITagDao tagDao;

	@Test
	void importiereJahrKlasse1() {

		// Arrange
		final String wettbewerbsjahr = "1999";
		final Klassenstufe klassenstufe = Klassenstufe.EINS;
		final String dateiname = "aufgaben.tex";

		// Act
		try {
			service.importAufgaben(wettbewerbsjahr, klassenstufe, dateiname);

			// Assert
			final List<Aufgabe> aufgaben = aufgabeDao.findAll(Aufgabe.class);
			assertEquals(15, aufgaben.size());

			for (final Aufgabe aufgabe : aufgaben) {
				System.out.println(aufgabe.print());
			}

			final List<Tag> tags = tagDao.findAll(Tag.class);
			assertEquals(6, tags.size());
		} catch (PreconditionFailedException | QuizException | IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	void importiereJahrKlasse2() {

		// Arrange
		final String wettbewerbsjahr = "1999";
		final Klassenstufe klassenstufe = Klassenstufe.ZWEI;
		final String dateiname = "aufgaben.tex";

		// Act
		try {
			service.importAufgaben(wettbewerbsjahr, klassenstufe, dateiname);

			// Assert
			final List<Aufgabe> aufgaben = aufgabeDao.findAll(Aufgabe.class);
			assertEquals(15, aufgaben.size());

			for (final Aufgabe aufgabe : aufgaben) {
				System.out.println(aufgabe.print());
			}

			final List<Tag> tags = tagDao.findAll(Tag.class);
			assertEquals(6, tags.size());
		} catch (PreconditionFailedException | QuizException | IOException e) {
			fail(e.getMessage());
		}
	}
}
