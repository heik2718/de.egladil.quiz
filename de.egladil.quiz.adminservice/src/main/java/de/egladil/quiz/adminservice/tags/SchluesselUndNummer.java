//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.tags;

/**
 * SchluesselUndNummer
 */
public class SchluesselUndNummer {

	private final String schluessel;

	private final String nummer;

	/**
	 * SchluesselUndNummer
	 */
	public SchluesselUndNummer(final String schluessel, final String nummer) {
		this.schluessel = schluessel;
		this.nummer = nummer;
	}

	public final String getSchluessel() {
		return schluessel;
	}

	public final String getNummer() {
		return nummer;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("SchluesselUndNummer [schluessel=");
		builder.append(schluessel);
		builder.append(", nummer=");
		builder.append(nummer);
		builder.append("]");
		return builder.toString();
	}

}
