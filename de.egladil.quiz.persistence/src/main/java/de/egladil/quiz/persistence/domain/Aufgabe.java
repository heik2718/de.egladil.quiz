//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import de.egladil.common.persistence.IDomainObject;

/**
 * Aufgabe
 */
@Entity
@Table(name = "aufgaben")
public class Aufgabe implements IDomainObject {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Version
	@Column(name = "VERSION")
	private int version;

	@Column
	private String schluessel;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "aufgaben_tagwerte", joinColumns = { @JoinColumn(name = "AUFGABE_ID") }, inverseJoinColumns = {
		@JoinColumn(name = "TAGWERT_ID") })
	private List<Tagwert> tagwerte = new ArrayList<>();

	/**
	 * Aufgabe
	 */
	public Aufgabe() {
	}

	/**
	 * Aufgabe
	 */
	public Aufgabe(final String schluessel) {
		this.schluessel = schluessel;
	}

	public void addWert(final Tagwert wert) {
		if (wert == null) {
			throw new IllegalArgumentException("wert null");
		}
		if (!tagwerte.contains(wert)) {
			tagwerte.add(wert);
		}
	}

	public void removeWert(final Tagwert wert) {
		if (wert == null) {
			throw new IllegalArgumentException("wert null");
		}
		if (tagwerte.contains(wert)) {
			tagwerte.remove(wert);
		}
	}

	@Override
	public final Long getId() {
		return id;
	}

	public final void setId(final Long id) {
		this.id = id;
	}

	public final String getSchluessel() {
		return schluessel;
	}

	public final void setSchluessel(final String schluessel) {
		this.schluessel = schluessel;
	}

	public final List<Tagwert> getTagwerte() {
		return tagwerte;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((schluessel == null) ? 0 : schluessel.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Aufgabe other = (Aufgabe) obj;
		if (schluessel == null) {
			if (other.schluessel != null) {
				return false;
			}
		} else if (!schluessel.equals(other.schluessel)) {
			return false;
		}
		return true;
	}

	public String print() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Aufgabe [schluessel=");
		builder.append(schluessel);
		builder.append(", tagwerte=");
		for (int i = 0; i < tagwerte.size(); i++) {
			final Tagwert wert = tagwerte.get(i);
			builder.append("'");
			builder.append(wert.toString());
			builder.append("'");
			if (i < tagwerte.size() - 1) {
				builder.append(", ");
			}
		}
		builder.append("]");
		return builder.toString();
	}
}
