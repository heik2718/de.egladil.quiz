//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import de.egladil.common.persistence.IDomainObject;

/**
 * Tagwert
 */
@Entity
@Table(name = "tagwerte")
public class Tagwert implements IDomainObject {

	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Version
	@Column(name = "VERSION")
	private int version;

	@Column(name = "WERT")
	private String wert;

	@Column(name = "KOMMENTAR")
	private String kommentar;

	@ManyToOne
	@JoinColumn(name = "TAG_ID")
	private Tag tag;

	/**
	 * Tagwert
	 */
	public Tagwert() {
	}

	/**
	 * Tagwert
	 */
	public Tagwert(final String wert) {
		this.wert = wert;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	public final void setId(final Long id) {
		this.id = id;
	}

	void onAddToTag(final Tag tag) {
		this.tag = tag;
	}

	void onRemoveFromTag() {
		this.tag = null;
	}

	public final String getWert() {
		return wert;
	}

	public final void setWert(final String wert) {
		this.wert = wert;
	}

	public final String getKommentar() {
		return kommentar;
	}

	public final void setKommentar(final String kommentar) {
		this.kommentar = kommentar;
	}

	public final Tag getTag() {
		return tag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		result = prime * result + ((wert == null) ? 0 : wert.toLowerCase().hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Tagwert other = (Tagwert) obj;
		if (tag == null) {
			if (other.tag != null) {
				return false;
			}
		} else if (!tag.equals(other.tag)) {
			return false;
		}
		if (wert == null) {
			if (other.wert != null) {
				return false;
			}
		} else {
			if (other.wert == null) {
				return false;
			} else {
				if (!wert.toLowerCase().equals(other.wert.toLowerCase())) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return wert;
	}
}
