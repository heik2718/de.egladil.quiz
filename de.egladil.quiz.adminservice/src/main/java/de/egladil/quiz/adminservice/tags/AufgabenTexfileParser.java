//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.tags;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.quiz.persistence.exceptions.QuizException;

/**
 * AufgabenTexfileParser parsed ein Minikänguru-Aufgaben-LaTeX-File um die Aufgaben mit den passenden Tags zu erzeugen.
 */
public class AufgabenTexfileParser {

	private static final Logger LOG = LoggerFactory.getLogger(AufgabenTexfileParser.class);

	private static final Pattern SCHLUESSEL_PATTERN = Pattern.compile("\\d{5}");

	private static final Pattern ZAHL_MINUS_ZAHL_PATTERN = Pattern.compile("\\d{1}\\-\\d{1}");

	private static final Pattern BUCHSTABE_MINUS_ZAHL_PATTERN = Pattern.compile("[A-C]{1}\\-\\d{1}");

	// private final Tag aufgabennummerTag = new Tag("Aufgabennummer");
	//
	// private final Tagwert wertAufgabenart;
	//
	// private final Tagwert wertSchwierigkeitsgrad;

	/**
	 * AufgabenTexfileParser
	 */
	public AufgabenTexfileParser() {
		// final Tag aufgabenartTag = new Tag("Aufgabenart");
		// aufgabenartTag.setKommentar("offen, multiple choice");
		// wertAufgabenart = new Tagwert("multiple choice");
		// aufgabenartTag.addWert(wertAufgabenart);
		//
		// final Tag schwierigkeitsgradQuizTag = new Tag("Schwierigkeitsgrad Quiz");
		// schwierigkeitsgradQuizTag.setKommentar("MINI = bis Klasse 4, MEDIUM = Klassen 5 bis 8, MAXI = ab Klasse 5,
		// MIXED für alle");
		// wertSchwierigkeitsgrad = new Tagwert("MINI");
		// schwierigkeitsgradQuizTag.addWert(wertSchwierigkeitsgrad);
	}

	/**
	 * Ermittelt alle Nummer-Schluessel-Paare aus einer LaTeX-Datei.
	 *
	 * @param pathFile String absoluter Pfad zur Datei.
	 * @return List
	 * @throws IOException falls die Datei nicht existiert, nicht gelesen werden kann, ...
	 * @throws QuizException falls sich nummer und schluessel nicht aus der gleichen Zeile ermitteln lassen.
	 */
	public List<SchluesselUndNummer> getAufgaben(final String pathFile) throws IOException, QuizException {

		try (InputStream in = new FileInputStream(new File(pathFile))) {
			return getAufgaben(in);
		}

	}

	List<SchluesselUndNummer> getAufgaben(final InputStream in) throws IOException, QuizException {

		final List<SchluesselUndNummer> result = new ArrayList<>();

		final BufferedReader br = new BufferedReader(new InputStreamReader(in, Charset.forName("UTF-8")));
		String line = null;

		while ((line = br.readLine()) != null) {
			if (line.startsWith("{\\bf Aufgabe ") || line.startsWith("{\\changefont{pag}{b}{n} ")) {

				LOG.debug(line);
				final String schluessel = getSchluessel(SCHLUESSEL_PATTERN, line);

				String nummer = null;
				if (line.contains("Aufgabe")) {
					nummer = getAufgabennummer(ZAHL_MINUS_ZAHL_PATTERN, line);
					final char first = nummer.charAt(0);
					switch (first) {
					case '3':
						nummer = "A" + nummer.substring(1, nummer.length());
						break;
					case '4':
						nummer = "B" + nummer.substring(1, nummer.length());
						break;
					case '5':
						nummer = "C" + nummer.substring(1, nummer.length());
						break;
					default:
						nummer = "falsch";
						break;
					}
				} else {
					nummer = getAufgabennummer(BUCHSTABE_MINUS_ZAHL_PATTERN, line);
				}
				if (schluessel != null && nummer != null) {
					result.add(new SchluesselUndNummer(schluessel, nummer));
				} else {
					LOG.error("Zeile: {}", line);
					if (schluessel == null) {
						throw new QuizException("Fehler in Datei: konnte Schlüssel nicht ermitteln.");
					}
					if (nummer == null) {
						throw new QuizException("Fehler in Datei: konnte Nummer nicht ermitteln.");
					}

				}
			}
		}

		return result;
	}

	private String getSchluessel(final Pattern pattern, final String line) {
		final Matcher matcher = pattern.matcher(line);
		while (matcher.find()) {
			final int startIndex = matcher.start();
			final int endIndex = matcher.end();

			final String schluessel = line.substring(startIndex, endIndex);
			return schluessel;
		}
		return null;
	}

	private String getAufgabennummer(final Pattern pattern, final String line) {
		final Matcher matcher = pattern.matcher(line);
		while (matcher.find()) {
			final int startIndex = matcher.start();
			final int endIndex = matcher.end();

			final String nummer = line.substring(startIndex, endIndex);
			return nummer;
		}
		return null;
	}

}
