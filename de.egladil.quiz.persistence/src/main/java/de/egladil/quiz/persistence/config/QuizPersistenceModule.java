//=====================================================
// Projekt: de.egladil.quiz.persistence.config
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.config;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.common.persistence.JpaPersistPrivateModule;
import de.egladil.common.persistence.protokoll.IEreignisDao;
import de.egladil.quiz.persistence.dao.IAufgabeDao;
import de.egladil.quiz.persistence.dao.ITagDao;
import de.egladil.quiz.persistence.dao.QuizPersistenceUnit;
import de.egladil.quiz.persistence.dao.impl.AufgabeDaoImpl;
import de.egladil.quiz.persistence.dao.impl.EreignisDaoImpl;
import de.egladil.quiz.persistence.dao.impl.TagDaoImpl;

public class QuizPersistenceModule extends JpaPersistPrivateModule {

	public QuizPersistenceModule(final String pathConfigRoot) {
		super(QuizPersistenceUnit.class, pathConfigRoot);
	}

	@Override
	protected String getPersistenceUnitName() {
		return "quizPU";
	}

	@Override
	protected IEgladilConfiguration getConfigurations() {
		return new AbstractEgladilConfiguration(getPathConfigRoot()) {

			/* serialVersionUID */
			private static final long serialVersionUID = 1L;

			@Override
			protected String getConfigFileName() {
				return "quiz_persistence.properties";
			}
		};
	}

	@Override
	protected void doConfigure() {

		bind(IEreignisDao.class).to(EreignisDaoImpl.class);
		expose(IEreignisDao.class);

		bind(ITagDao.class).to(TagDaoImpl.class);
		expose(ITagDao.class);

		bind(IAufgabeDao.class).to(AufgabeDaoImpl.class);
		expose(IAufgabeDao.class);
	}
}
