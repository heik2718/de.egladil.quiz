//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.resources;

import java.net.MalformedURLException;
import java.net.URL;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.quiz.adminservice.service.ImportWettbewerbService;
import de.egladil.quiz.persistence.domain.Klassenstufe;

/**
 * ImportWettbewerbeResource ist zum Import der Aufgaben eines Wettbewerbs gedacht. Es wird ein LaTeX- File eingelesen
 * und daraus Aufgaben mit Tagwerten generiert.
 */
@Singleton
@Path("/wettbewerbe")
public class ImportWettbewerbeResource {

	private static final Logger LOG = LoggerFactory.getLogger(ImportWettbewerbeResource.class);

	private URL mainUrl;

	private final ImportWettbewerbService importWettbewerbService;

	private final ResourceExceptionHandler exceptionHandler;

	/**
	 * ImportWettbewerbeResource
	 */
	@Inject
	public ImportWettbewerbeResource(final ImportWettbewerbService importWettbewerbService) {
		this.importWettbewerbService = importWettbewerbService;
		this.exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

		try {
			mainUrl = new URL("https://mathe-jung-alt.de");
		} catch (final MalformedURLException e) {
			LOG.error(e.getMessage());
		}
	}

	@POST
	@Path("/jahr/klasse/dateiname")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	public Response importAufgaben(@PathParam(value = "jahr") final String wettbewerbsjahr,
		@PathParam(value = "klasse") final int klasse, @PathParam(value = "dateiname") final String dateiname) {

		Klassenstufe klassenstufe = null;
		try {
			klassenstufe = Klassenstufe.valueOfNummer(klasse);

			importWettbewerbService.importAufgaben(wettbewerbsjahr, klassenstufe, dateiname);

			final APIResponsePayload payload = new APIResponsePayload(APIMessage.info("Wettbewerb erfolgreich importiert"));
			final URL url = new URL(mainUrl, "/quizadmin/wettbewerbe/" + wettbewerbsjahr + "/" + klasse);

			return Response.created(url.toURI()).entity(payload).build();
		} catch (final EgladilAuthenticationException | DisabledAccountException e) {
			LOG.warn("GET /laender: {}", e.getMessage());
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final IllegalArgumentException e) {
			final APIResponsePayload payload = new APIResponsePayload(APIMessage.error(e.getMessage()));
			return Response.status(412).entity(payload).build();
		} catch (final PreconditionFailedException e) {
			final APIResponsePayload payload = new APIResponsePayload(
				APIMessage.error("Wettbewerb " + wettbewerbsjahr + " Klassenstufe " + klassenstufe + " bereits importiert"));
			return Response.status(412).entity(payload).build();
		} catch (final Throwable e) {
			LOG.error(e.getMessage(), e);
			return this.exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}
}
