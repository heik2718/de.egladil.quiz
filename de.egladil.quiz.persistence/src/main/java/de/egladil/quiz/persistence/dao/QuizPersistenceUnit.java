//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.dao;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

@Documented
@Qualifier
@Retention(RUNTIME)
@Target({ TYPE })
/**
 * QuizPersistenceUnit für die Quiz-DB.
 */
public @interface QuizPersistenceUnit {
}
