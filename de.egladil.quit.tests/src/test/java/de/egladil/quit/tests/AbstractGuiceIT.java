//=====================================================
// Projekt: de.egladil.quit.tests
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quit.tests;

import org.junit.jupiter.api.BeforeEach;

import com.google.inject.Guice;
import com.google.inject.Injector;

import de.egladil.common.config.OsUtils;
import de.egladil.quit.tests.config.TestModule;
import de.egladil.quiz.adminservice.config.DynamicAdminConfigReader;

/**
 * @author heikew
 *
 */
public abstract class AbstractGuiceIT {

	@BeforeEach
	public void setUp() {
		final Injector injector = Guice.createInjector(new TestModule(OsUtils.getDevConfigRoot()));
		injector.injectMembers(this);

		DynamicAdminConfigReader.getInstance().init(OsUtils.getDevConfigRoot());
	}
}
