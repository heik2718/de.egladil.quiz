//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.config;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.name.Names;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.IBenutzerService;
import de.egladil.bv.aas.IRegistrierungService;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.impl.CachingAccessTokenAuthenticator;
import de.egladil.bv.aas.auth.impl.CachingAccessTokenDao;
import de.egladil.bv.aas.config.BVPersistenceModule;
import de.egladil.bv.aas.impl.AuthenticationServiceImpl;
import de.egladil.bv.aas.impl.BenutzerServiceImpl;
import de.egladil.bv.aas.impl.RegistrierungServiceImpl;
import de.egladil.common.config.IEgladilConfiguration;
import de.egladil.crypto.provider.IEgladilCryptoUtils;
import de.egladil.crypto.provider.impl.EgladilCryptoUtilsImpl;
import de.egladil.quiz.adminservice.service.ImportWettbewerbService;
import de.egladil.quiz.adminservice.service.impl.ImportWettbewerbServiceImpl;
import de.egladil.quiz.persistence.config.QuizPersistenceModule;
import io.dropwizard.auth.Authenticator;

/**
 * MKVModule
 */
public class QuizAdminModule extends AbstractModule {

	private final String pathConfigRoot;

	/**
	 * Erzeugt eine Instanz von QuizAdminModule
	 */
	public QuizAdminModule(final String pathConfigRoot) {
		this.pathConfigRoot = pathConfigRoot;
	}

	@Override
	protected void configure() {
		loadProperties(binder());

		install(new BVPersistenceModule(pathConfigRoot));
		install(new QuizPersistenceModule(pathConfigRoot));

		bind(IAuthenticationService.class).to(AuthenticationServiceImpl.class);
		bind(IBenutzerService.class).to(BenutzerServiceImpl.class);
		bind(IRegistrierungService.class).to(RegistrierungServiceImpl.class);
		bind(Authenticator.class).to(CachingAccessTokenAuthenticator.class);

		bind(IAccessTokenDAO.class).to(CachingAccessTokenDao.class);

		bind(IEgladilConfiguration.class).to(QuizAdminConfiguration.class);
		bind(IEgladilCryptoUtils.class).to(EgladilCryptoUtilsImpl.class);

		bind(ImportWettbewerbService.class).to(ImportWettbewerbServiceImpl.class);
	}

	protected void loadProperties(final Binder binder) {
		final Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", pathConfigRoot);
		Names.bindProperties(binder, properties);
	}
}
