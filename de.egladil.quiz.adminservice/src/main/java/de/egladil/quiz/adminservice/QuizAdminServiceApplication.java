//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice;

import java.security.Principal;
import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;

import org.eclipse.jetty.servlet.ErrorPageErrorHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Singleton;
import com.google.inject.persist.PersistService;

import de.egladil.bv.aas.auth.CsrfFilter;
import de.egladil.bv.aas.storage.BVPersistenceUnit;
import de.egladil.common.persistence.EgladilPersistFilter;
import de.egladil.common.webapp.AngularJsonVulnerabilityProtectionInterceptor;
import de.egladil.common.webapp.exception.EgladilLoggingExceptionMapper;
import de.egladil.common.webapp.exception.JsonProcessingExceptionMapper;
import de.egladil.common.webapp.exception.LoggingConstraintViolationExceptionMapper;
import de.egladil.quiz.adminservice.config.QuizAdminModule;
import de.egladil.quiz.adminservice.config.QuizAdminServiceConfiguration;
import de.egladil.quiz.adminservice.health.BVHealthCheck;
import de.egladil.quiz.adminservice.health.PingHealthCheck;
import de.egladil.quiz.adminservice.resources.ImportWettbewerbeResource;
import de.egladil.quiz.adminservice.resources.PingResource;
import de.egladil.quiz.persistence.dao.QuizPersistenceUnit;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter;
import io.dropwizard.jersey.errors.EarlyEofExceptionMapper;
import io.dropwizard.setup.Environment;

/**
 * QuizAdminServiceApplication
 */
public class QuizAdminServiceApplication extends Application<QuizAdminServiceConfiguration> {

	private static final Logger LOG = LoggerFactory.getLogger(QuizAdminServiceApplication.class);

	private static final String SERVICE_NAME = "MKV-ADMIN-API";

	private static final String BEARER_PREFIX = "Bearer";

	@Singleton
	public static class JPAInitializer {

		@Inject
		public JPAInitializer(final PersistService service) {
			service.start();
		}
	}

	public static void main(final String[] args) {
		try {
			final QuizAdminServiceApplication application = new QuizAdminServiceApplication();
			application.run(args);
		} catch (final Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * @see io.dropwizard.Application#getName()
	 */
	@Override
	public String getName() {
		return SERVICE_NAME;
	}

	/**
	 * @see io.dropwizard.Application#run(io.dropwizard.Configuration, io.dropwizard.setup.Environment)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run(final QuizAdminServiceConfiguration configuration, final Environment environment) throws Exception {
		LOG.info("configRoot: {}", configuration.getConfigRoot());
		configureCors(configuration, environment);
		configureErrorPages(environment);
		configureExceptionMappers(environment);

		final Injector injector = Guice.createInjector(new QuizAdminModule(configuration.getConfigRoot()));

		initPersistFilter(environment, injector);

		// authenticator ist hier ein CachingAccessTokenAuthenticator
		final Authenticator<String, Principal> authenticator = injector.getInstance(Authenticator.class);
		final OAuthCredentialAuthFilter<Principal> oAuthFilter = new OAuthCredentialAuthFilter.Builder<Principal>()
			.setAuthenticator(authenticator).setPrefix(BEARER_PREFIX).buildAuthFilter();
		environment.jersey().register(new AuthDynamicFeature(oAuthFilter));
		// der Parameter Principal.class in new AuthValueFactoryProvider.Binder<>(Principal.class) sorgt dafür, dass der
		// vom Authenticator zuückgegebene Principal als Parameter
		// in die Methode der REST-Resource-Klassenstufe gesetzt wird. Muss übereinstimmen mit
		// OAuthCredentialAuthFilter.Builder<Principal>! Sonst ist der Parameter null!!!
		environment.jersey().register(new AuthValueFactoryProvider.Binder<>(Principal.class));
		environment.jersey().register(RolesAllowedDynamicFeature.class);
		environment.jersey().register(MultiPartFeature.class);
		environment.jersey().register(AngularJsonVulnerabilityProtectionInterceptor.class);

		registerCsrfFilter(environment, injector);

		final BVHealthCheck bvHealthCheck = injector.getInstance(BVHealthCheck.class);
		environment.healthChecks().register("BV", bvHealthCheck);

		final PingResource pingResource = injector.getInstance(PingResource.class);
		environment.jersey().register(pingResource);
		environment.healthChecks().register("ping", new PingHealthCheck(pingResource));

		environment.jersey().register(injector.getInstance(ImportWettbewerbeResource.class));
	}

	/**
	 * Statt der standard-Jetty-Htmlseiten wird hier Plain Old JSON ohne sensible Informationen zurückgegeben
	 *
	 * @param environment
	 */
	private void configureErrorPages(final Environment environment) {
		final ErrorPageErrorHandler errorHandler = new ErrorPageErrorHandler();
		errorHandler.addErrorPage(401, "/errorpages/401");
		errorHandler.addErrorPage(404, "/errorpages/404");
		errorHandler.addErrorPage(500, "/errorpages/500");
		errorHandler.addErrorPage(900, "/errorpages/900");
		environment.getApplicationContext().setErrorHandler(errorHandler);
	}

	/**
	 * Registriert adaptierte ExceptionMapper aus io.dropwizard.server.AbstractServerFactory.
	 *
	 * @param environment
	 */
	private void configureExceptionMappers(final Environment environment) {
		environment.jersey().register(new EgladilLoggingExceptionMapper());
		environment.jersey().register(new JsonProcessingExceptionMapper());
		environment.jersey().register(new LoggingConstraintViolationExceptionMapper());
		environment.jersey().register(new EarlyEofExceptionMapper());
	}

	private void configureCors(final QuizAdminServiceConfiguration configuration, final Environment environment) {
		// ab Dropwizard 0.8.1 erforderlich
		System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
		final FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORSFilter", CrossOriginFilter.class);

		filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, configuration.getAllowedOrigins());
		filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Content-Type,Accept,Authorization,Origin,X-XSRF-TOKEN");
		filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
		filter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
		// filter.setInitParameter(CrossOriginFilter.EXPOSED_HEADERS_PARAM, "true");

		// Add URL mapping
		// FIXME: secure?
		filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
	}

	private void initPersistFilter(final Environment environment, final Injector injector) {
		final EgladilPersistFilter pfBV = injector.getInstance(Key.get(EgladilPersistFilter.class, BVPersistenceUnit.class));
		final EgladilPersistFilter pfQuiz = injector.getInstance(Key.get(EgladilPersistFilter.class, QuizPersistenceUnit.class));

		final FilterRegistration.Dynamic bvFilter = environment.servlets().addFilter("BVPersistFilter", pfBV);
		final FilterRegistration.Dynamic quizFilter = environment.servlets().addFilter("QuizPersistFilter", pfQuiz);

		bvFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");
		quizFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");
	}


	/**
	 * Registriert einen CSRF-Filter
	 *
	 * @param environment
	 * @param injector
	 * @param test
	 */
	private void registerCsrfFilter(final Environment environment, final Injector injector) {
		final CsrfFilter csrfFilter = injector.getInstance(CsrfFilter.class);

		final FilterRegistration.Dynamic filter = environment.servlets().addFilter("csrfFilter", csrfFilter);
		filter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), false, "/*");
	}
}
