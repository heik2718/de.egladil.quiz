//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quit.tests.config;

import java.util.HashMap;
import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.name.Names;

import de.egladil.bv.aas.config.BVPersistenceModule;
import de.egladil.quiz.adminservice.service.ImportWettbewerbService;
import de.egladil.quiz.adminservice.service.impl.ImportWettbewerbServiceImpl;
import de.egladil.quiz.persistence.config.QuizPersistenceModule;

/**
 * MKVModule
 */
public class TestModule extends AbstractModule {

	private final String configRoot;

	/**
	 * Erzeugt eine Instanz von TestModule
	 */
	public TestModule(final String configRoot) {
		this.configRoot = configRoot;
	}

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
	@Override
	protected void configure() {
		loadProperties(binder());
		install(new BVPersistenceModule(this.configRoot));
		install(new QuizPersistenceModule(this.configRoot));

		bind(ImportWettbewerbService.class).to(ImportWettbewerbServiceImpl.class);
	}

	/**
	 *
	 * @param binder
	 */
	protected void loadProperties(final Binder binder) {
		final Map<String, String> properties = new HashMap<>();
		properties.put("configRoot", this.configRoot);
		Names.bindProperties(binder, properties);
	}
}
