//=====================================================
// Projekt: de.egladil.mkv.service
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.common.persistence.protokoll.Ereignis;
import de.egladil.common.persistence.protokoll.IEreignisDao;
import de.egladil.quiz.persistence.dao.QuizPersistenceUnit;
import de.egladil.quiz.persistence.exceptions.QuizException;

/**
 * EreignisDaoImpl
 */
@Singleton
@QuizPersistenceUnit
public class EreignisDaoImpl extends BaseDaoImpl<Ereignis> implements IEreignisDao {

	private static final Logger LOG = LoggerFactory.getLogger(EreignisDaoImpl.class);

	/**
	 * Erzeugt eine Instanz von EreignisDaoImpl
	 */
	@Inject
	public EreignisDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public String delete(final Ereignis entity) {
		throw new QuizException("Ereignisse duerfen nicht geloescht werden!!!");
	}

	/**
	 * @see de.egladil.common.persistence.BaseDaoImpl#delete(java.util.List)
	 */
	@Override
	public void delete(final List<Ereignis> entities) {
		throw new QuizException("Ereignisse duerfen nicht geloescht werden!!!");
	}

	@Override
	public List<Ereignis> findByEreignisart(final String kuerzelEreignisart) {
		if (kuerzelEreignisart == null) {
			throw new IllegalArgumentException("kuerzelEreignisart null");
		}
		final String stmt = "SELECT e from Ereignis e where e.ereignisart = :value";
		LOG.debug(stmt);

		final EntityManager entityManager = getEntityManager();
		final TypedQuery<Ereignis> query = entityManager.createQuery(stmt, Ereignis.class);
		query.setParameter("value", kuerzelEreignisart);

		final List<Ereignis> treffer = query.getResultList();
		LOG.debug("Anzahl Treffer={}", treffer.size());
		return treffer;
	}
}
