//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.dao.impl;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.quiz.persistence.dao.ITagDao;
import de.egladil.quiz.persistence.dao.QuizPersistenceUnit;
import de.egladil.quiz.persistence.domain.Tag;

/**
 * TagDaoImpl
 */
@Singleton
@QuizPersistenceUnit
public class TagDaoImpl extends BaseDaoImpl<Tag> implements ITagDao {

	private static final Logger LOG = LoggerFactory.getLogger(TagDaoImpl.class);

	/**
	 * TagDaoImpl
	 */
	@Inject
	public TagDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

	@Override
	public List<Tag> findByNameSubstring(final String substring) {
		if (StringUtils.isBlank(substring)) {
			throw new IllegalArgumentException("substring blank");
		}

		final String stmt = "select t from Tag t where lower(t.name) like :substr";
		final TypedQuery<Tag> query = getEntityManager().createQuery(stmt, Tag.class);
		query.setParameter("substr", "%" + substring.toLowerCase() + "%");

		final List<Tag> trefferliste = query.getResultList();

		LOG.debug("Anzahl Treffer: {}", trefferliste.size());

		return trefferliste;
	}

}
