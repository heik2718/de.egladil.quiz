//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * TagTest
 */
public class TagTest {

	@Nested
	@DisplayName("Tests für equals")
	class EqualsTests {

		@Test
		@DisplayName("both names null")
		void nullNamesEqual() {
			final Tag tag1 = new Tag();
			final Tag tag2 = new Tag();

			assertTrue(tag1.equals(tag2));
			assertEquals(tag1.hashCode(), tag2.hashCode());
		}

		@Test
		@DisplayName("identical objects")
		void identicalObjects() {
			final Tag tag1 = new Tag();
			tag1.setName("sigdi");
			tag1.setId(1l);

			assertTrue(tag1.equals(tag1));
			assertEquals(tag1.hashCode(), tag1.hashCode());
		}

		@Test
		@DisplayName("other object null")
		void otherObjectNull() {
			final Tag tag1 = new Tag();
			tag1.setName("sigdi");
			tag1.setId(1l);

			assertFalse(tag1.equals(null));
		}

		@SuppressWarnings("unlikely-arg-type")
		@Test
		@DisplayName("uncomparable classes")
		void notSameClass() {
			final Tag tag1 = new Tag();
			tag1.setName("sigdi");
			tag1.setId(1l);

			assertFalse(tag1.equals(new Aufgabe()));
		}

		@Test
		@DisplayName("first name null")
		void firstNameNullOtherNameNotNull() {
			final Tag tag1 = new Tag();
			final Tag tag2 = new Tag();
			tag2.setName("sigdi");
			tag2.setId(1l);

			assertFalse(tag1.equals(tag2));
		}

		@Test
		@DisplayName("other name null")
		void firstNameNotNullOtherNameNull() {
			final Tag tag1 = new Tag();
			tag1.setName("sajba");
			final Tag tag2 = new Tag();
			tag2.setId(2l);

			assertFalse(tag1.equals(tag2));
		}

		@Test
		@DisplayName("names completely different")
		void completelyDifferentNames() {
			final Tag tag1 = new Tag();
			tag1.setName("sajba");
			final Tag tag2 = new Tag();
			tag2.setName("hajhu");
			tag2.setId(2l);

			assertFalse(tag1.equals(tag2));
		}

		@Test
		@DisplayName("names completely different")
		void sameNamesCaseSensitive() {
			final Tag tag1 = new Tag();
			tag1.setName("Hallo");
			final Tag tag2 = new Tag();
			tag2.setName("hallo");
			tag2.setId(2l);

			assertTrue(tag1.equals(tag2));
			assertEquals(tag1.hashCode(), tag2.hashCode());
		}
	}
}
