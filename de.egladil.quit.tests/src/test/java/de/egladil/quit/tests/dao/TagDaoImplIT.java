//=====================================================
// Projekt: de.egladil.quit.tests
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quit.tests.dao;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import javax.inject.Inject;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import de.egladil.quit.tests.AbstractGuiceIT;
import de.egladil.quiz.persistence.config.QuizConstants;
import de.egladil.quiz.persistence.dao.ITagDao;
import de.egladil.quiz.persistence.domain.Tag;
import de.egladil.quiz.persistence.domain.Tagwert;

/**
 * TagDaoImplIT
 */
public class TagDaoImplIT extends AbstractGuiceIT {

	@Inject
	private ITagDao dao;

	@Test
	void findByUniqueKeyKlappt() {

		final Optional<Tag> opt = dao.findByUniqueKey(Tag.class, QuizConstants.NAME_ATTRIBUTE_NAME, "foo");

		assertFalse(opt.isPresent());
		// final Tag tag = opt.get();
		//
		// assertEquals(2, tag.getWerte().size());
	}

	@Disabled
	void persistKlappt() {
		// Arrange
		final Tag tag = new Tag("Foo");

		{
			final Tagwert tagwert = new Tagwert("Wert 1");
			tag.addWert(tagwert);
		}

		{
			final Tagwert tagwert = new Tagwert("Wert 2");
			tag.addWert(tagwert);
		}

		// Act
		final Tag persisted = dao.persist(tag);

		// Assert
		assertNotNull(persisted);
	}

}
