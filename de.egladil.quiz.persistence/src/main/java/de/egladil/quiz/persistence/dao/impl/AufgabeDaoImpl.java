//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.dao.impl;

import javax.persistence.EntityManager;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import de.egladil.common.persistence.BaseDaoImpl;
import de.egladil.quiz.persistence.dao.IAufgabeDao;
import de.egladil.quiz.persistence.dao.QuizPersistenceUnit;
import de.egladil.quiz.persistence.domain.Aufgabe;

/**
* AufgabeDaoImpl
*/
@Singleton
@QuizPersistenceUnit
public class AufgabeDaoImpl extends BaseDaoImpl<Aufgabe> implements IAufgabeDao {



	/**
	* AufgabeDaoImpl
	*/
	@Inject
	public AufgabeDaoImpl(final Provider<EntityManager> emp) {
		super(emp);
	}

}
