//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.bv.aas.IAuthenticationService;
import de.egladil.bv.aas.auth.AccessToken;
import de.egladil.bv.aas.auth.AccessTokenUtils;
import de.egladil.bv.aas.auth.AuthenticationInfo;
import de.egladil.bv.aas.auth.IAccessTokenDAO;
import de.egladil.bv.aas.auth.UsernamePasswordToken;
import de.egladil.bv.aas.domain.Anwendung;
import de.egladil.bv.aas.domain.Benutzerkonto;
import de.egladil.common.exception.DisabledAccountException;
import de.egladil.common.exception.EgladilAuthenticationException;
import de.egladil.common.persistence.EgladilConcurrentModificationException;
import de.egladil.common.webapp.APIMessage;
import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.SessionToken;

/**
 * SessionDelegate
 */
@Singleton
public class SessionDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(SessionDelegate.class);

	private final IAccessTokenDAO accessTokenDAO;

	private final IAuthenticationService authenticationService;

	/**
	 * Erzeugt eine Instanz von SessionDelegate
	 */
	@Inject
	public SessionDelegate(final IAccessTokenDAO accessTokenDAO, final IAuthenticationService authenticationService) {
		this.accessTokenDAO = accessTokenDAO;
		this.authenticationService = authenticationService;
	}

	/**
	 *
	 * @param usernamePasswordToken
	 * @param tempCsrfToken
	 * @return
	 */
	public SessionToken authenticate(final UsernamePasswordToken usernamePasswordToken, final String tempCsrfToken)
		throws IllegalArgumentException, EgladilAuthenticationException, DisabledAccountException,
		EgladilConcurrentModificationException {
		final AuthenticationInfo authenticationInfo = authenticationService.authenticate(usernamePasswordToken, Anwendung.MKV);
		final Benutzerkonto benutzer = (Benutzerkonto) authenticationInfo;
		final String benutzerUuid = benutzer.getUuid();
		final AccessToken accessToken = accessTokenDAO.generateNewAccessToken(authenticationInfo);

		LOG.info("{} logged in", benutzerUuid);

		invalidateTemporaryCsrfTokenQuietly(tempCsrfToken);
		return new SessionToken(accessToken.getAccessTokenId(), accessToken.getCsrfToken());
	}

	public String logoutQuietly(final String benutzerUuid, final String bearer) {
		invalidateSessionQuietly(bearer);
		LOG.info("{} logged in", benutzerUuid);
		return "";
	}

	/**
	 * Entfernt die SessionId.
	 *
	 * @param accessTokenId
	 */
	public void invalidateSessionQuietly(final String bearer) {
		try {
			final String accessTokenId = new AccessTokenUtils().extractAccessTokenId(bearer);
			accessTokenDAO.invalidateAccessToken(accessTokenId);
		} catch (final Throwable e) {
			LOG.error("Session konnte nicht entwertet werden: " + e.getMessage(), e);
		}
	}

	/**
	 * Entfernt das temporäre Csrf-Token.
	 *
	 * @param csrfToken
	 */
	void invalidateTemporaryCsrfTokenQuietly(final String csrfToken) {
		try {
			accessTokenDAO.invalidateTemporaryCsrfToken(csrfToken);
		} catch (final Exception e) {
			LOG.error("Session konnte nicht entwertet werden: " + e.getMessage(), e);
		}
	}

	/**
	 *
	 *
	 * @return APIResponsePayload mit SessionToken
	 * @throws Exception
	 */
	public APIResponsePayload createAnonymousSession() throws Exception {

		final String csrfToken = authenticationService.createCsrfToken();
		accessTokenDAO.registerTemporaryCsrfToken(csrfToken);
		final SessionToken sessionToken = new SessionToken(null, csrfToken);
		final APIResponsePayload entity = new APIResponsePayload(APIMessage.info("ok"));
		entity.setSessionToken(sessionToken);
		return entity;
	}
}
