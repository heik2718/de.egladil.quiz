//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.config;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.egladil.common.config.EgladilConfigurationException;

/**
 *
 * DynamicAdminConfigReader
 */
public class DynamicAdminConfigReader {
	private static final String NAME_EXTERNAL_KONTEXT_FILE = "quizadmin.json";

	private static final Logger LOG = LoggerFactory.getLogger(DynamicAdminConfigReader.class);

	private String pathConfigFile;

	private static DynamicAdminConfigReader instance;

	private DynamicAdminConfig mockConfig;

	/**
	 * DynamicAdminConfigReader
	 */
	DynamicAdminConfigReader() {
	}

	public static final DynamicAdminConfigReader getInstance() {
		if (instance == null) {
			instance = new DynamicAdminConfigReader();
		}
		return instance;
	}

	public DynamicAdminConfigReader init(final String pathConfigRoot) {
		if (this.pathConfigFile == null) {
			this.pathConfigFile = pathConfigRoot + File.separator + NAME_EXTERNAL_KONTEXT_FILE;
			// Das Ding wirft eine EgladilConfigurationException, die das hochfahren des Servers unterbricht.
			// Damit wird sichergestellt, dass alle weiteren Zugriffe klappen, solange die Konfig-Datei nicht
			// verschwindet.
			this.getConfig();
		}
		return this;
	}

	public DynamicAdminConfig getConfig() throws EgladilConfigurationException {
		if (mockConfig != null) {
			return mockConfig;
		}
		if (this.pathConfigFile == null) {
			throw new IllegalStateException(
				"DynamicAdminConfigReader ist nicht initialisiert. Beim Start der Anwendung einmal DynamicAdminConfigReader.getInstance().init(...) aufrufen!");
		}
		final File configFile = new File(pathConfigFile);
		final ObjectMapper objectMapper = new ObjectMapper();
		DynamicAdminConfig config = null;
		try {
			config = objectMapper.readValue(configFile, DynamicAdminConfig.class);
			return config;
		} catch (final IOException e) {
			LOG.debug(e.getMessage(), e);
			throw new EgladilConfigurationException("Konnte Konfigurationsfile [" + pathConfigFile + "] nicht lesen:  "
				+ e.getMessage()
				+ " folgendes prüfen: DynamicAdminConfig.NAME_EXTERNAL_KONTEXT_FILE und configRoot in mkvservice-deb.yml. NAME_EXTERNAL_KONTEXT_FILE="
				+ NAME_EXTERNAL_KONTEXT_FILE, e);
		}
	}

	/**
	 * Singletons sind manchmal nervig und müssen zerstört werden können.
	 */
	public static void destroy() {
		instance = null;
	}

	public void setMockKontextForTest(final DynamicAdminConfig config) {
		this.mockConfig = config;
	}
}
