//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.dao;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.quiz.persistence.domain.Aufgabe;

/**
* IAufgabeDao
*/
public interface IAufgabeDao extends IBaseDao<Aufgabe> {

}
