//=====================================================
// Projekt: de.egladil.quiz.persistence.config
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.config;

/**
 * QuizConstants
 */
public interface QuizConstants {

	String ID_ATTRIBUTE_NAME = "id";

	String KUERZEL_ATTRIBUTE_NAME = "kuerzel";

	String SCHLUESSEL_ATTRIBUTE_NAME = "schluessel";

	String NAME_ATTRIBUTE_NAME = "name";

	String UUID_ATTRIBUTE_NAME = "uuid";

	int DEFAULT_LENGTH = 8;

	char[] DEFAULT_CHARS = "ABCDEFGHIJKLMNOPQRTSUVWXYZ0123456789".toCharArray();

	String TAGNAME_AUFGABENNUMMER = "Aufgabennummer";

	String TAGNAME_WETTBEWERBSJAHR = "Wettbewerbsjahr";

	String TAGNAME_KLASSENSTUFE = "Klassenstufe Minikänguru";

	String TAGNAME_AUFGABENKATEGORIE = "Aufgabenkategorie Minikänguru";

	String TAGNAME_SCHWIERIGKEITSGRAD = "Schwierigkeitsgrad Quiz";

	String TAGNAME_AUFGABENART = "Aufgabenart";
}
