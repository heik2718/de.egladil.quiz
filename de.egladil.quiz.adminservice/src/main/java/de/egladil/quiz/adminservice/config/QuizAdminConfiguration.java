//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.config;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.IEgladilConfiguration;


/**
 * @author heikew
 *
 */
@Singleton
public class QuizAdminConfiguration extends AbstractEgladilConfiguration implements IEgladilConfiguration {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von EgladilMKVConfiguration
	 */
	@Inject
	public QuizAdminConfiguration(@Named("configRoot") final String pathConfigRoot) {
		super(pathConfigRoot);
	}

	@Override
	protected String getConfigFileName() {
		return "quiz_admin.properties";
	}
}
