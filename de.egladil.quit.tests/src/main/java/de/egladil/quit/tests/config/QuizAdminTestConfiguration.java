//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quit.tests.config;

import de.egladil.common.config.AbstractEgladilConfiguration;
import de.egladil.common.config.OsUtils;

/**
 * QuizAdminTestConfiguration
 */
public class QuizAdminTestConfiguration extends AbstractEgladilConfiguration {
	/* serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt eine Instanz von QuizAdminTestConfiguration
	 */
	public QuizAdminTestConfiguration() {
		this(OsUtils.getDevConfigRoot());

	}

	/**
	 * Erzeugt eine Instanz von QuizAdminTestConfiguration
	 */
	public QuizAdminTestConfiguration(final String pathConfigRoot) {
		super(OsUtils.getDevConfigRoot());
	}

	@Override
	protected String getConfigFileName() {
		return "quiz_admin.properties";
	}

};