//=====================================================
// Projekt: de.egladil.quiz.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.dao;

import java.util.List;

import de.egladil.common.persistence.IBaseDao;
import de.egladil.quiz.persistence.domain.Tag;

/**
 * ITagDao
 */
public interface ITagDao extends IBaseDao<Tag> {

	/**
	 * Sucht Tags mit dem gegebenen substring
	 *
	 * @param substring String darf nicht blank sein.
	 * @return
	 */
	List<Tag> findByNameSubstring(String substring);
}
