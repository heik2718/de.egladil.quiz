//=====================================================
// Projekt: de.egladil.mkv.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.exception.PreconditionFailedException;
import de.egladil.quiz.adminservice.config.DynamicAdminConfig;
import de.egladil.quiz.adminservice.config.DynamicAdminConfigReader;
import de.egladil.quiz.adminservice.service.ImportWettbewerbService;
import de.egladil.quiz.adminservice.tags.AufgabenTexfileParser;
import de.egladil.quiz.adminservice.tags.SchluesselUndNummer;
import de.egladil.quiz.persistence.config.QuizConstants;
import de.egladil.quiz.persistence.dao.IAufgabeDao;
import de.egladil.quiz.persistence.dao.ITagDao;
import de.egladil.quiz.persistence.domain.Aufgabe;
import de.egladil.quiz.persistence.domain.Aufgabenkategorie;
import de.egladil.quiz.persistence.domain.Klassenstufe;
import de.egladil.quiz.persistence.domain.Tag;
import de.egladil.quiz.persistence.domain.Tagwert;
import de.egladil.quiz.persistence.exceptions.QuizException;

/**
 * ImportWettbewerbServiceImpl
 */
@Singleton
public class ImportWettbewerbServiceImpl implements ImportWettbewerbService {

	private static final Logger LOG = LoggerFactory.getLogger(ImportWettbewerbServiceImpl.class);

	private final IAufgabeDao aufgabeDao;

	private final ITagDao tagDao;

	private List<Tagwert> gemeinsameTagwerte = new ArrayList<>();

	/**
	 * ImportWettbewerbServiceImpl
	 */
	@Inject
	public ImportWettbewerbServiceImpl(final IAufgabeDao aufgabeDao, final ITagDao tagDao) {
		this.aufgabeDao = aufgabeDao;
		this.tagDao = tagDao;
	}

	@Override
	public void importAufgaben(final String wettbewerbsjahr, final Klassenstufe klassenstufe, final String dateiname)
		throws PreconditionFailedException, QuizException, IOException {

		final Optional<Tag> optTagWettbewerbsjahr = tagDao.findByUniqueKey(Tag.class, QuizConstants.NAME_ATTRIBUTE_NAME,
			QuizConstants.TAGNAME_WETTBEWERBSJAHR);
		final Optional<Tag> optTagKlassenstufe = tagDao.findByUniqueKey(Tag.class, QuizConstants.NAME_ATTRIBUTE_NAME,
			QuizConstants.TAGNAME_KLASSENSTUFE);

		boolean wettbewerbsjahrFound = false;
		boolean klassenstufeFound = false;
		if (optTagWettbewerbsjahr.isPresent() && optTagWettbewerbsjahr.get().findWert(wettbewerbsjahr).isPresent()) {
			wettbewerbsjahrFound = true;
		}
		if (optTagKlassenstufe.isPresent() && optTagKlassenstufe.get().findWert(klassenstufe.toString()).isPresent()) {
			klassenstufeFound = true;
		}
		if (wettbewerbsjahrFound && klassenstufeFound) {
			throw new PreconditionFailedException(
				"Wettbewerbsjahr " + wettbewerbsjahr + " und Klassenstufe " + klassenstufe + " bereits importiert");
		}

		final DynamicAdminConfig config = DynamicAdminConfigReader.getInstance().getConfig();
		final String path = config.getImportverzeichnisWettbewerb() + File.separator + dateiname;

		final List<SchluesselUndNummer> schluesselUndNummern = new AufgabenTexfileParser().getAufgaben(path);

		gemeinsameTagwerte.clear();
		initGemeinsameTagwerte(wettbewerbsjahr, klassenstufe);

		for (final SchluesselUndNummer schluesselUndNummer : schluesselUndNummern) {
			Aufgabe aufgabe = findOrCreateAufgabe(schluesselUndNummer.getSchluessel());
			aufgabe = taggeAufgabe(aufgabe, schluesselUndNummer);
			LOG.info("{} importiert: {}", schluesselUndNummer, aufgabe.print());
		}
	}

	private void initGemeinsameTagwerte(final String wettbewerbsjahr, final Klassenstufe klassenstufe) {
		{
			final String tagname = QuizConstants.TAGNAME_WETTBEWERBSJAHR;
			final Tag tag = findOrCreateTag(tagname, wettbewerbsjahr, "gibt es nur beim Minikänguruwettbewerb", null);
			final Optional<Tagwert> optWert = tag.findWert(wettbewerbsjahr);
			if (!optWert.isPresent()) {
				throw new QuizException("Fehler Ablauf Import: Tagwert " + wettbewerbsjahr + " existiert noch nicht");
			}
			gemeinsameTagwerte.add(optWert.get());
		}
		{
			final String tagname = QuizConstants.TAGNAME_KLASSENSTUFE;
			final Tag tag = findOrCreateTag(tagname, klassenstufe.toString(), "Klassenstufen beim Minikänguruwettbewerb", null);
			final Optional<Tagwert> optWert = tag.findWert(klassenstufe.toString());
			if (!optWert.isPresent()) {
				throw new QuizException("Fehler Ablauf Import: Tagwert " + klassenstufe + " existiert noch nicht");
			}
			gemeinsameTagwerte.add(optWert.get());
		}
		{
			final String tagname = QuizConstants.TAGNAME_SCHWIERIGKEITSGRAD;
			final String wert = "MINI";
			final Tag tag = findOrCreateTag(tagname, wert,
				"für freie Quiz gibt es 4 Schwierigkeitsgrade, die mehrere Klassenstufen zusammenfassen.", "bis Klasse 4");
			final Optional<Tagwert> optWert = tag.findWert(wert);
			if (!optWert.isPresent()) {
				throw new QuizException("Fehler Ablauf Import: Tagwert " + wert + " existiert noch nicht");
			}
			gemeinsameTagwerte.add(optWert.get());
		}
		{
			final String tagname = QuizConstants.TAGNAME_AUFGABENART;
			final String wert = "multiple choice";
			final Tag tag = findOrCreateTag(tagname, wert, "offene Aufgaben, multiple choice- Aufgaben, ...", null);
			final Optional<Tagwert> optWert = tag.findWert(wert);
			if (!optWert.isPresent()) {
				throw new QuizException("Fehler Ablauf Import: Tagwert " + wert + " existiert noch nicht");
			}
			gemeinsameTagwerte.add(optWert.get());
		}
	}

	private Aufgabe findOrCreateAufgabe(final String schluessel) {

		final Optional<Aufgabe> optAufgabe = aufgabeDao.findByUniqueKey(Aufgabe.class, QuizConstants.SCHLUESSEL_ATTRIBUTE_NAME,
			schluessel);

		Aufgabe aufgabe = null;
		if (optAufgabe.isPresent()) {
			aufgabe = optAufgabe.get();
		} else {
			aufgabe = new Aufgabe();
			aufgabe.setSchluessel(schluessel);
		}

		final Aufgabe persisted = aufgabeDao.persist(aufgabe);
		return persisted;
	}

	private Aufgabe taggeAufgabe(final Aufgabe aufgabe, final SchluesselUndNummer schluesselUndNummer) {
		for (final Tagwert tagwert : gemeinsameTagwerte) {
			aufgabe.addWert(tagwert);
		}
		{
			final Aufgabenkategorie aufgabenkategorie = Aufgabenkategorie.getByNummer(schluesselUndNummer.getNummer());
			final String wert = aufgabenkategorie.toString();
			final Tag tag = findOrCreateTag(QuizConstants.TAGNAME_AUFGABENKATEGORIE, wert,
				"Minikänguru kennt 3 Aufgabenkategorien A, B und C", aufgabenkategorie.getNummerPrefix());
			final Optional<Tagwert> optWert = tag.findWert(wert);
			if (!optWert.isPresent()) {
				throw new QuizException("Fehler Ablauf Import: Tagwert " + wert + " existiert noch nicht");
			}
			aufgabe.addWert(optWert.get());
		}
		{
			final String wert = schluesselUndNummer.getNummer();
			final Tag tag = findOrCreateTag(QuizConstants.TAGNAME_AUFGABENNUMMER, wert,
				"Nummer der Aufgabe in einer Sammlung von Aufgaben, z.B. Minikänguruwettbewerb oder Quiz", null);
			final Optional<Tagwert> optWert = tag.findWert(wert);
			if (!optWert.isPresent()) {
				throw new QuizException("Fehler Ablauf Import: Tagwert " + wert + " existiert noch nicht");
			}
			aufgabe.addWert(optWert.get());
		}

		final Aufgabe persisted = aufgabeDao.persist(aufgabe);

		return persisted;

	}

	private Tag findOrCreateTag(final String tagname, final String wert, final String kommentarTag, final String kommentarTagwert) {
		final Optional<Tag> optTag = tagDao.findByUniqueKey(Tag.class, QuizConstants.NAME_ATTRIBUTE_NAME, tagname);

		Tag tag = null;
		if (optTag.isPresent()) {
			tag = optTag.get();
		} else {
			tag = new Tag(tagname);
			tag.setKommentar(kommentarTag);
		}
		final Optional<Tagwert> optTagwert = tag.findWert(wert);
		if (!optTagwert.isPresent()) {
			final Tagwert tagwert = new Tagwert(wert);
			tagwert.setKommentar(kommentarTagwert);
			tag.addWert(tagwert);
		}

		final Tag persisted = tagDao.persist(tag);
		return persisted;
	}
}
