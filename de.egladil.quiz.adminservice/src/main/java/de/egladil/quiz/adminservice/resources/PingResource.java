//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.egladil.common.webapp.APIResponsePayload;
import de.egladil.common.webapp.ResourceExceptionHandler;
import de.egladil.crypto.provider.EgladilEncryptionException;
import de.egladil.quiz.adminservice.session.SessionDelegate;

/**
 *
 * PingResource ist ein Beispiel für beschränkte Zugriffe auf REST-Resourcen. Die Annotation @Auth sorgt zusammen mit
 * der Initialisierung der EgladilBVApplication dafür, dass der AccessTokenAuthenticator.authenticate() aufgerufen wird,
 * bevor eine zugriffsbeschränkte Methode aufgerufen wird. Klassen wie PingResource werden in Clients dieses
 * Microservices verwendet.
 */
@Singleton
@Path("/csrftoken")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class PingResource {

	private static final Logger LOG = LoggerFactory.getLogger(PingResource.class);

	private final SessionDelegate sessionDelegate;

	private final ResourceExceptionHandler exceptionHandler = new ResourceExceptionHandler(new UniqueIndexNameMapper());

	/**
	 * Erzeugt eine Instanz von PingResource
	 */
	@Inject
	public PingResource(final SessionDelegate sessionDelegate) {
		this.sessionDelegate = sessionDelegate;
	}

	/**
	 * Dient zur CSRF Prävention vor dem Login. Erzeugt ein temporäres CSRF-Token.
	 *
	 * @return Map
	 */
	@GET
	@Timed
	public Response getTemporarySession() {

		try {
			final APIResponsePayload entity = sessionDelegate.createAnonymousSession();
		// @formatter:off
		return Response
			.ok()
			.entity(entity)
			.type(MediaType.APPLICATION_JSON)
			.encoding("UTF-8")
			.build();
        // @formatter:on
		} catch (final EgladilEncryptionException e) {
			LOG.error("temp CSRF-Token konnte nicht generiert werden: {}", e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		} catch (final Exception e) {
			LOG.error("Unerwartete Exception: {}", e.getMessage(), e);
			return exceptionHandler.mapToMKVApiResponse(e, null, null);
		}
	}

}
