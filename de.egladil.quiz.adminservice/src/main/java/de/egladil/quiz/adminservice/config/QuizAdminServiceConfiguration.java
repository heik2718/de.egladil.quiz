//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.config;

import io.dropwizard.Configuration;

/**
 * QuizAdminServiceConfiguration
 */
public class QuizAdminServiceConfiguration extends Configuration {

	private String configRoot;

	private String allowedOrigins;

	/**
	 * Erzeugt eine Instanz von QuizAdminServiceConfiguration
	 */
	public QuizAdminServiceConfiguration() {
	}

	public String getConfigRoot() {
		return configRoot;
	}

	public String getAllowedOrigins() {
		return allowedOrigins;
	}

	public void setConfigRoot(final String configRoot) {
		this.configRoot = configRoot;
	}
}
