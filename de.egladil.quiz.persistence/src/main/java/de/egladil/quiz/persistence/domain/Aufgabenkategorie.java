//=====================================================
// Projekt: de.egladil.mkv.persistence
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.persistence.domain;

import de.egladil.quiz.persistence.exceptions.QuizException;

/**
 * Aufgabenkategorie
 */
public enum Aufgabenkategorie {

	LEICHT("A"),
	MITTEL("B"),
	SCHWER("C");

	private final String nummerPrefix;

	/**
	 * Aufgabenkategorie
	 *
	 * @param nummerPrefix
	 */
	private Aufgabenkategorie(final String nummerPrefix) {
		this.nummerPrefix = nummerPrefix;
	}

	public static Aufgabenkategorie getByNummer(final String nummer) {
		if (nummer == null) {
			throw new IllegalArgumentException("nummer null");
		}
		for (final Aufgabenkategorie kategorie : Aufgabenkategorie.values()) {
			if (nummer.startsWith(kategorie.nummerPrefix)) {
				return kategorie;
			}
		}
		throw new QuizException("fehlerhafte Nummernsyntax '" + nummer + "': nummer muss mit A, B oder C beginnen");
	}

	public final String getNummerPrefix() {
		return nummerPrefix;
	}

}
