//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.config;

/**
* DynamicAdminConfig
*/
public class DynamicAdminConfig {

	private String importverzeichnisWettbewerb;

	public final String getImportverzeichnisWettbewerb() {
		return importverzeichnisWettbewerb;
	}

	public final void setImportverzeichnisWettbewerb(final String importverzeichnisWettbewerb) {
		this.importverzeichnisWettbewerb = importverzeichnisWettbewerb;
	}
}
