//=====================================================
// Projekt: de.egladil.quiz.adminservice
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.quiz.adminservice.resources;

import java.util.Optional;

import de.egladil.common.persistence.EgladilDuplicateEntryException;
import de.egladil.common.webapp.UniqueIndexNameMessageProvider;

/**
* UniqueIndexNameMapper
*/
public class UniqueIndexNameMapper implements UniqueIndexNameMessageProvider {

	/**
	* UniqueIndexNameMapper
	*/
	public UniqueIndexNameMapper() {
	}

	@Override
	public Optional<String> exceptionToMessage(final EgladilDuplicateEntryException e) {
		return null;
	}

}
